def longest_valid(s):
    """ longest_valid(string) -> string """
    # Finds largest valid sequence of parentheses and brackets in a string.
    # Solved using dynamic programming. Go through the array recording the longest valid
    # match ending from each index. If you've got the longest match for index i, then
    # it's easy to find the longest match for index i+1: skip backwards the longest match
    # for index i, and then see if the characters surrounding that are matching open/close
    # brackets. Then add the longest match to the left of that too, if any.
    match = [0] * (len(s) + 1)
    for i in range(1, len(s)):
        if s[i] in '({[':
            continue
        open = '({['[')}]'.index(s[i])]
        start = i - 1 - match[i - 1]
        if start < 0: continue
        if s[start] != open: continue
        match[i] = i - start + 1 + match[start - 1]
    best = max(match)
    end = match.index(best)
    return s[end + 1 - best:end + 1]
