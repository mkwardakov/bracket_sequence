# Bracket sequence demo task

Task: Find largest valid sequence of parentheses and brackets in a string

Example: ((}{))()[()]

Answer: ()[()]
