import main

def test_1():
    assert main.longest_valid("()(({}[](][{[()]}]{})))(") == "[{[()]}]{}"

def test_2():
    assert main.longest_valid("()(({}[]([{[()]}]{})))(")  == "()(({}[]([{[()]}]{})))"

def test_3():
    assert main.longest_valid("{}[()()()()()()()]")       == "{}[()()()()()()()]"

def test_4():
    assert main.longest_valid("((}{))()[()]")             == "()[()]"
